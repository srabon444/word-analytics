export default function Stats({ stats }) {
  const {
    numberOfCharacters,
    numberOfWords,
    numberOfSentence,
    numberOfParagraphs,
    facebookCharactersLeft,
    instagramCharactersLeft,
  } = stats;

  return (
    <section className="stats">
      <Stat number={numberOfCharacters} label="Characters" />
      <Stat number={numberOfWords} label="Words" />
      <Stat number={numberOfSentence} label="Sentences" />
      <Stat number={numberOfParagraphs} label="Paragraphs" />
      <Stat number={facebookCharactersLeft} label="Facebook" />
      <Stat number={instagramCharactersLeft} label="Instagram" />
    </section>
  );
}

function Stat({ number, label }) {
  return (
    <section className="stat">
      <span
        className={`stat_number ${number < 0 ? "stat__number--limit" : ""}`}
      >
        {number}
      </span>
      <h2 className="second-heading">{label}</h2>
    </section>
  );
}
