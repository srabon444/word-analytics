import TextArea from "./TextArea.jsx";
import Stats from "./Stats.jsx";
import { useState } from "react";
import {
  FACEBOOK_MAX_CHARACTERS,
  INSTAGRAM_MAX_CHARACTERS,
} from "../lib/constants.js";

export default function Container() {
  const [text, setText] = useState("");

  const stats = {
    numberOfCharacters: text.length,
    numberOfWords: text.split(/\s/).filter((word) => word !== "").length,
    numberOfSentence: text
      .split(/[.!?]/)
      .filter((sentence) => sentence.trim() !== "").length,
    numberOfParagraphs: text
      .split(/\n\s*\n/)
      .filter((paragraph) => paragraph.trim() !== "").length,
    instagramCharactersLeft: INSTAGRAM_MAX_CHARACTERS - text.length,
    facebookCharactersLeft: FACEBOOK_MAX_CHARACTERS - text.length,
  };

  return (
    <main className="container">
      <TextArea text={text} setText={setText} />
      <Stats stats={stats} />
    </main>
  );
}
