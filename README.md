# Word Analytics

This Word Analytics is created using React.js with Vite, Javascript, and CSS


## [Live Application](https://word-analytics-woad.vercel.app/)

![App Screenshot 1](public/1.png)
![App Screenshot 2](public/2.png)
![App Screenshot 3](public/3.png)

## Features

- Characters, Words, Sentences, and Paragraphs start counting in the stats section.
- Facebook and Instagram character limit decreases while typing.
- When the limit goes bellow zero, character count becomes red as a warning
- If user wants to type @ or, script inside a script tag, it stops user and shows warning bellow.

## Tech Stack
- React Js
- Javascript
- CSS

**Hosting:**
- Vercel


## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/srabon444/word-analytics.git
```

Install dependencies

```bash
  npm install
```


